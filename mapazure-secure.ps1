# Created by igal cohen
# date : 29.10.2023
# Version : 0.0.8
# Objective: Decrpyt AES Password and Map Azure Network Storage Instance.

$aesKey = (2,3,1,4,54,32,144,23,5,3,1,41,36,31,18,175,6,17,1,9,5,1,76,23)
 # set string
$passwordfile = "c:\scripts\Password.txt"
$myfile = get-content $passwordfile

# create new object using $myfile and $aeskey
$secureObject = ConvertTo-SecureString -String $myfile -Key $aesKey
 
# perform decryption from secure object
$decrypted = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureObject)
$decrypted = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($decrypted)


cmdkey /add:<storageAccountName>.file.core.windows.net /user:<Azure User Name> /pass:$decrypted 

net use l: \\<storageAccountName>.file.core.windows.net\<fileShareName>

