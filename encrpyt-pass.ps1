# Created by igal cohen
# date : 29.10.2023
# Version : 0.0.25
# Objective: Encrypt Secure String password with key usable in multiple enviromnets.
# Remarks: this kind of encryption will prevent you from the error when using the secure string in another machine
# due to the fact that If the -Key parameter is not specified, then the Windows Data Protection API secures the string. 
# This string can only be decrypted by the same user on the same machine.



# create aes key - keep this secure at all times
$aesKey = (2,3,1,4,54,32,144,23,5,3,1,41,36,31,18,175,6,17,1,9,5,1,76,23)
 # set string
$plaintext = "<StorageAccountKey>" 
clear-host
 
 
# convert to secure string object
$Secure = ConvertTo-SecureString -String $plaintext -AsPlainText -Force
 
$passwordfile = "c:\scripts\Password.txt"
# store secure object - use ouput in the decryption process. Could be saved to file.
# remember, the aeskey should remain physically secured
$encrypted = ConvertFrom-SecureString -SecureString $Secure -Key $aesKey | out-file $passwordfile
Write-Host "Encrypted:`n$encrypted`n"

$myfile = get-content $passwordfile