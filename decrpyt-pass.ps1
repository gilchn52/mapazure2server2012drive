# Created by igal cohen
# Date 29.10.23
# Objective : Decrypt Powershell Secure String with Key to Plain Text 

# create aes key - keep this secure at all times
$aesKey = (2,3,1,4,54,32,144,23,5,3,1,41,36,31,18,175,6,17,1,9,5,1,76,23)
 # set string
$myfile = get-content $passwordfile

# create new object using $myfile and $aeskey
$secureObject = ConvertTo-SecureString -String $myfile -Key $aesKey
 
# perform decryption from secure object
$decrypted = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureObject)
$decrypted = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($decrypted)
$decrypted