# MapAzure2Server2012Drive



## Getting started

Objective: This project includes 3 files The Mapping Azure Storage instance , the Encryption of the secure string with key and The Decryption of the secure string .

 ## Mapping 
 - Decrpyt AES Password and Map Azure Network Storage Instance.

 ## Encrypt

 - Encrypt Secure String password with key usable in multiple enviromnets.
   Remarks: this kind of encryption will prevent you from the error when using the secure string in another machine
   due to the fact that If the -Key parameter is not specified, then the Windows Data Protection API secures the string. 
   This string can only be decrypted by the same user on the same machine.

 ## Decrypt

 - Decrypt Powershell Secure String with Key to Plain Text 


